import sys
sys.path.insert(0, 'api')

import api

def test_login():
    response = api.login()
    assert response.status_code == 201
    assert "jott007@web.de" in str(response.json())

def test_project_list():
    response = api.project_list()
    assert response.status_code == 200
    assert "python-movies" in str(response.json())
