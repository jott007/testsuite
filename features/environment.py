import logging
import os

from selenium import webdriver

def before_all(context):
    context.browser = webdriver.PhantomJS(service_log_path=os.path.devnull)

def after_all(context):
    # context.browser.quit()
    os.system('pgrep phantomjs | xargs kill')
