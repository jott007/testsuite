"""A module that interacts with the GitLab API"""
import os
import requests

BASE_URL = "https://gitlab.com/api/v3"

def login():
    """Logs a user into GitLab"""
    return requests.post(BASE_URL + '/session?login=' + os.environ['GITLAB_USER'] + '&password=' + os.environ['GITLAB_PW'])

def project_list():
    """Lists all projects for the logged in user"""
    headers = {'PRIVATE-TOKEN': os.environ['GITLAB_TOKEN']}
    return requests.get(BASE_URL + '/projects', headers=headers)
